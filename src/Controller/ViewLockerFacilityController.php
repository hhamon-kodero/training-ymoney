<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\LockerFacility;
use App\Repository\LockerFacilityRepository;
use App\Repository\ParcelLockerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ViewLockerFacilityController extends AbstractController
{
    public function __construct(
        private readonly LockerFacilityRepository $lockerFacilityRepository,
        private readonly ParcelLockerRepository $parcelLockerRepository,
    ) {
    }

    #[Route(path: '/facilities/{name<[a-z]+>}', name: 'app_view_locker_facility', methods: 'GET', )]
    public function __invoke(string $name): Response
    {
        $facility = $this->lockerFacilityRepository->findOneCommissionedByName($name);

        if (! $facility instanceof LockerFacility) {
            throw $this->createNotFoundException();
        }

        return $this->render('locker_facility/view.html.twig', [
            'facility' => $facility,
            'lockers' => $this->parcelLockerRepository->findByFacility($facility),
        ]);
    }
}
